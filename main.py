from concurrent import futures
from random import randint

import grpc

import protos.post.post_pb2 as pb2
import protos.post.post_pb2_grpc as pb2_grpc

database_stub = []


class PostService(pb2_grpc.PostServicer):
    def CreatePost(self, request, context):
        result = {"id": randint(1, 10000), "title": request.title, "description": request.description}
        database_stub.append(result)

        return pb2.CreatePostResponseMessage(**result)

    def RetrievePosts(self, request, context):
        return pb2.RetrievePostsResponseMessageList(posts=database_stub)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    pb2_grpc.add_PostServicer_to_server(PostService(), server)
    server.add_insecure_port("[::]:50051")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    serve()
